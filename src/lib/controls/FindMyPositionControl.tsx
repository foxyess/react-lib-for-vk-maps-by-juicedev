import {IControl, LngLatLike, Map} from 'mmr-gl';

export interface FindMyPositionControlConstructorOptions {
  defaultCenter: LngLatLike;
  element?: HTMLElement;
}

export class FindMyPositionControl implements IControl {
  _map?: Map;
  _container!: HTMLDivElement;
  defaultCenter?: LngLatLike;
  element?: HTMLElement;

  constructor({defaultCenter, element}: FindMyPositionControlConstructorOptions) {
    this.defaultCenter = defaultCenter;
    this.element = element;
  }

  getButtonElement() {
    const button = document.createElement('button');
    button.textContent = 'go to me';

    button.onclick = () => {
      this._map?.flyTo({
        center: this.defaultCenter,
        zoom: 20,
      });
    };

    return button;
  }

  onAdd(map: Map) {
    this._map = map;
    this._container = document.createElement('div');
    this._container.className = 'mmrgl-ctrl custom--mmrgl-ctrl-stretched';

    const button = this.getButtonElement();

    this._container.appendChild(button);

    return this._container;
  }

  onRemove() {
    this._container.parentNode?.removeChild(this._container);
    this._map = undefined;
  }
}