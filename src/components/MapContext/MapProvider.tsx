import {Map} from 'mmr-gl';
import React, {
  FC,
  ReactNode,
  useCallback,
  useEffect,
  useState,
} from 'react';

import {MapContext, MapContextValue} from './MapContext';

export interface MapProviderProps {
  children: ReactNode,
  value?: MapContextValue,
}

export const MapProvider: FC<MapProviderProps> = ({children, value}) => {
  const [map, setMap] = useState<Map | null>(null);
  const [isReady, setIsReady] = useState(false);

  const onLoad = useCallback(() => {
    setIsReady(true);
  }, []);

  const onRemove = useCallback(() => {
    setIsReady(false);
  }, []);

  useEffect(() => {
    if (!map) return;

    map.on('load', onLoad);
    map.on('remove', onRemove);

    return () => {
      map.off('load', onLoad);
      map.off('remove', onRemove);
    };
  }, [map, onLoad, onRemove]);

  return (
    <MapContext.Provider
      value={{
        map,
        setMap,
        isReady,
      }}
    >
      {children}
    </MapContext.Provider>
  );
};
