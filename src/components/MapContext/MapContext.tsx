import {Map} from 'mmr-gl';
import {createContext} from 'react';

export interface MapContextValue {
  map: Map | null,
  setMap: (map: Map | null) => void,
  isReady: boolean,
}

export const MapContext = createContext<MapContextValue>({
  map: null,
  setMap: () => {},
  isReady: false,
});