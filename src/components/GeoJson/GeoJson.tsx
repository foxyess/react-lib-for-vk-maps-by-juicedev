import {FC, memo, useEffect, useMemo} from 'react';
import {Expression, GeoJSONSourceRaw} from 'mmr-gl';

import {useMapContext} from '../../lib';

export interface GeoJsonProps {
  id: string,
  source: GeoJSONSourceRaw,
  fillColor?: string | Expression,
  fillOpacity?: number | Expression,
  borderColor?: string | Expression,
  borderSize?: number | Expression,
}

export const GeoJson: FC<GeoJsonProps> = memo((
    {
      source,
      id,
      fillColor = 'blue',
      fillOpacity = 0.13,
      borderColor = fillColor,
      borderSize = 3,
    },
) => {
  const {map, isReady} = useMapContext();

  const fillLayerId = useMemo(() => `${id}-fill`, [id]);
  const borderLayerId = useMemo(() => `${id}-border`, [id]);

  useEffect(() => {
    if (!map || !isReady) return;

    !map.getSource(id) && map.addSource(id, source);

    !map.getLayer(fillLayerId) && map.addLayer({
      id: fillLayerId,
      type: 'fill',
      source: id,
      paint: {
        'fill-color': fillColor,
        'fill-opacity': fillOpacity,
      },
    });

    !map.getLayer(borderLayerId) && map.addLayer({
      id: borderLayerId,
      type: 'line',
      source: id,
      paint: {
        'line-color': borderColor,
        'line-width': borderSize,
      },
    });

    return () => {
      try {
        map.getLayer(fillLayerId) && map.removeLayer(fillLayerId);
        map.getLayer(borderLayerId) &&
        map.removeLayer(borderLayerId);
        map.getSource(id) && map.removeSource(id);
      } catch (error) {
      }
    };
  }, [
    map,
    id,
    fillColor,
    fillOpacity,
    fillLayerId,
    borderColor,
    borderLayerId,
    borderSize,
    isReady,
  ]);

  useEffect(() => {
    if (!map || !isReady) return;

    const mapSource = map.getSource(id);
    const hasFillLayer = map.getLayer(fillLayerId);
    const hasBorderLayer = map.getLayer(borderLayerId);

    if (
        mapSource &&
        map.isSourceLoaded(id) &&
        hasFillLayer &&
        hasBorderLayer &&
        mapSource.type === 'geojson'
    ) {
      mapSource.setData(source.data as string);
      return;
    }

    !mapSource && map.addSource(id, source);

    !hasFillLayer && map.addLayer({
      id: fillLayerId,
      type: 'fill',
      source: id,
      paint: {
        'fill-color': fillColor,
        'fill-opacity': fillOpacity,
      },
    });

    !hasBorderLayer && map.addLayer({
      id: borderLayerId,
      type: 'line',
      source: id,
      paint: {
        'line-color': borderColor,
        'line-width': borderSize,
      },
    });
  }, [
    map,
    source,
    id,
    fillLayerId,
    borderLayerId,
    fillColor,
    fillOpacity,
    borderColor,
    borderSize,
    isReady,
  ]);

  return null;
});
