import React, {CSSProperties, FC, ReactNode, useEffect, useRef} from 'react';
import mmrgl, {LngLatLike, MMROptions} from 'mmr-gl';

import 'mmr-gl/dist/mmr-gl.css';

import {useMapContext} from '../../lib';

export type MapColorMode = 'dark' | 'light' | 'simple';

export interface MapProps {
  token: string,
  children?: ReactNode,
  initialCenter?: LngLatLike,
  initialZoom?: number,
  mode?: MapColorMode,
  width?: CSSProperties['width'],
  height?: CSSProperties['height'],
  logoPosition?: MMROptions['logoPosition'],
}

const styles: { [key in MapColorMode]: string } = {
  dark: 'mmr://api/styles/dark_style.json',
  light: 'mmr://api/styles/main_style.json',
  simple: 'mmr://api/styles/simple_style.json',
};

export const Map: FC<MapProps> = (
    {
      token,
      mode = 'light',
      width = '100%',
      height = '100%',
      initialCenter,
      initialZoom,
      children,
      logoPosition = 'bottom-left',
    },
) => {
  const {setMap, map} = useMapContext();
  const mapRef = useRef<HTMLDivElement | null>(null);

  // Инициализация карты
  useEffect(() => {
    if (!mapRef.current) return;

    mmrgl.accessToken = token;

    const map = new mmrgl.Map({
      container: mapRef.current,
      style: styles[mode],
      logoPosition: logoPosition,
    });

    setMap(map);

    return () => {
      if (map) {
        map.remove();
        setMap(null);
      }
    };
  }, [token]);

  useEffect(() => {
    if (!map) return;

    map.setStyle(styles[mode]);
    initialCenter && map.setCenter(initialCenter);
    initialZoom && map.setZoom(initialZoom);
  }, [map, mode, initialCenter, initialZoom]);

  return (
      <div
          ref={mapRef}
          style={{position: 'absolute', width, height}}
      >
        {children}
      </div>
  );
};
