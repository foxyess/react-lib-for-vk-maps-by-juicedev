import {FC, ReactElement, useEffect, useRef} from 'react';
import {createRoot, Root} from 'react-dom/client';
import mmrgl from 'mmr-gl';

import {useMapContext} from '../../lib';

export interface MarkerProps {
  lon: number,
  lat: number,
  offsetX?: number,
  offsetY?: number,
  children?: ReactElement,
  onClick?: (e: MouseEvent, map: mmrgl.Map) => void,
}

export const Marker: FC<MarkerProps> = (
  {
    lon,
    lat,
    children,
    offsetX = 0,
    offsetY = 0,
    onClick,
  },
) => {
  const markerRef = useRef<mmrgl.Marker | null>(null);
  const rootRef = useRef<Root | null>(null);

  const {map, isReady} = useMapContext();

  useEffect(() => {
    if (rootRef.current) return;

    const element = document.createElement('div');
    rootRef.current = createRoot(element);

    markerRef.current = new mmrgl.Marker({
      element: element,
    });
  }, []);

  useEffect(() => {
    if (!map) {
      if (markerRef.current) {
        markerRef.current.remove();
      }
      return;
    }

    if (!isReady) return;

    const marker = markerRef.current;

    if (!rootRef.current || !marker) return;

    rootRef.current.render(children);

    marker.setOffset([offsetY, offsetX]);
    marker.setLngLat({lon, lat});
    marker.addTo(map);

    return () => {
      marker.remove();
    };
  }, [
    markerRef,
    rootRef,
    map,
    lon,
    lat,
    children,
    offsetY,
    offsetX,
    isReady,
  ]);

  useEffect(() => {
    if (!map) return;
    if (!onClick) return;

    const clickListener = (e: MouseEvent) => onClick(e, map);

    markerRef.current?.getElement()?.addEventListener('click', clickListener);

    return () => {
      markerRef.current?.getElement()?.removeEventListener('click', clickListener);
    };
  }, [markerRef, onClick, map]);

  return null;
};
