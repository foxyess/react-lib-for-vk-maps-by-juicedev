import {useContext} from 'react';

import {MapContext} from '../components';

export const useMapContext = () => useContext(MapContext);