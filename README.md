# Пример использования
```tsx
import React from 'react';
import mmrgl, {LngLatLike} from 'mmr-gl';
import ReactDOM from 'react-dom/client';

import {
  MapProvider,
  Map,
  Marker,
  useMapContext,
} from 'vk-maps-react';

const MARKERS = [
  {id: 1, lon: 37.6165, lat: 55.7505},
  {id: 2, lon: 37.6145, lat: 55.7505},
  {id: 3, lon: 37.6145, lat: 55.7525},
];

const App = () => {
  const {map} = useMapContext();

  const onMarkerClick = useCallback((position: LngLatLike) => {
    return (e: MouseEvent, map: mmrgl.Map) => {
      map.flyTo({
        center: position,
        zoom: 18,
      });
    };
  }, []);

  return (
      <div className='App'>
          <div style={{position: 'relative', width: '100dvw', height: '100dvh'}}>
              <Map
                  token={YOUR_VK_MAPS_TOKEN}
                  initialCenter={[37.6165, 55.7505]}
                  initialZoom={13}
                  mode={'dark'}
              >
              {
                MARKERS.map(({id, lat, lon}) => {
                  return (
                      <Marker
                          key={id}
                          lon={lon}
                          lat={lat}
                          onClick={onMarkerClick({lon, lat})}
                      />
                  );
                })
              }
              </Map>
          </div>
      </div>
  )
};

const root = ReactDOM.createRoot(
    document.getElementById('root') as HTMLElement,
);

root.render(
    <MapProvider>
        <App/>
    </MapProvider>,
);
```
